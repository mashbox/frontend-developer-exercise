# Front-end Developer Exercise

Hello There!

We hope you are as excited about data and APIs as we are here at [Mashbox](https://www.mashbox.com). There is nothing more satisfying than tinkering with code and creating something useful from all of the amazing information available. We are stoked that you will be showing us some of your skills and appreciate the time.

We created this exercise to show us how you think through and solve problems and what your go-to solutions are at this point in your career. This represents the type of items you may be asked to solve on a daily basis, and while we might have a few specific things we will look for in your code, this is not fizz-buzz so no code gotchas. There is no time limit to complete, as we all have busy lives and its totally understandable that you may only have limited times to work through it. That said, timeliness is a virtue in our line of work—so try not to get too caught up. Just follow these instructions  carefully and we will return the favor by providing some feedback.

## Your Mission

First, fork this repository.

Next take the Sketch design from the email we sent with this link and turn it into a functional application using HTML, CSS, and JS that can be run from a static file server such as Amazon S3. Sorry, no backends allowed for this one. If you do not have sketch, you can use the provided JPG of the same name and match to your best ability.

You can build the application in any way you like, using any technologies or libraries that are most comfortable to you—just make sure that whatever is used can be easily executed by a Mashbox developer. So make sure you save those dependencies. Create the application files in a directory named src and include all of the necessary original files (images, SASS, etc..). The application will need to receive it's data by making requests to the [SWAPI (Star Wars API)](https://swapi.co/) which is open source and available without any authentication. Like everything being created these days, this should be mobile responsive and fit down to an iPhone 5 portrait screen, there are no responsive designs provided—so use your best judgement.

Once completed, make sure that the production-ready files are in a subdirectory named dist as we will be uploading those files for the test. To submit, either send a link to your forked repository or zip your entire exercise directory naming it with your first initial plus last name, followed by _swapi_ and then the four digit year and two digit month (jdoe_swapi_201703), and send it to dev@mashbox.com with the subject line "My SWAPI Exercise"

Also, be sure to have some fun with it.

Looking for some bonus points? Try adding some of these:

 * Include Animations and transitions
 * Use SASS
 * Poll the endpoint for new information
 * Build it without a framework
 * Use a bundler
 * Do something in wookie format


*Copyright Mashbox LLC.*